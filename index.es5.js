'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _getValue = require('get-value');

var _getValue2 = _interopRequireDefault(_getValue);

var _objectHash = require('object-hash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // eslint-disable-next-line spaced-comment
/**!
 * @bsara/react-sort v2.0.1
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-filter/blob/master/LICENSE)
 */

var Sort = function (_React$Component) {
  _inherits(Sort, _React$Component);

  function Sort() {
    var _ref;

    _classCallCheck(this, Sort);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Sort.__proto__ || Object.getPrototypeOf(Sort)).call.apply(_ref, [this].concat(args)));

    _this._lastItems = undefined;
    _this._lastComparator = undefined;
    _this._lastComparators = undefined;
    _this._lastComparatorsHash = undefined;

    _this._cachedSortedItems = undefined;
    _this._cachedGeneratedComparatorConfigHash = undefined;
    _this._cachedGeneratedComparator = undefined;
    return _this;
  }

  _createClass(Sort, [{
    key: 'render',
    value: function render() {
      var renderFunc = this.props.children != null ? this.props.children : this.props.render;

      return renderFunc(this._sortItems());
    }

    // region Private Helpers

    /** @private */

  }, {
    key: '_sortItems',
    value: function _sortItems() {
      if (this.props.skip) {
        return this.props.items;
      }

      var comparatorsHash = this.props.comparators == null ? undefined : (0, _objectHash.MD5)(this.props.comparators);

      if (this.props.ignoreCache || this.props.items !== this._lastItems || this.props.comparator !== this._lastComparator || this.props.comparators !== this._lastComparators && (this._lastComparatorsHash == null || comparatorsHash !== this._lastComparatorsHash)) {
        this._setCachedSortedItems();

        this._lastItems = this.props.items;
        this._lastComparator = this.props.comparator;
        this._lastComparators = this.props.comparators;
        this._lastComparatorsHash = comparatorsHash;
      }

      return this._cachedSortedItems;
    }

    /** @private */

  }, {
    key: '_setCachedSortedItems',
    value: function _setCachedSortedItems() {
      var _this2 = this;

      if (this.props.items == null || !this.props.items.length || this.props.comparator == null && this.props.comparators == null) {
        this._cachedSortedItems = this.props.items;
        return;
      }

      var finalItems = [].concat(_toConsumableArray(this.props.items));
      var finalSorter = this.props.sorter || _defaultSorter;
      var finalComparator = this.props.comparator || this._getComparatorConfigsComparator();

      this._cachedSortedItems = finalSorter(finalItems, function () {
        for (var _len2 = arguments.length, sortComparatorArgs = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          sortComparatorArgs[_key2] = arguments[_key2];
        }

        return finalComparator.apply(undefined, sortComparatorArgs.concat([{
          defaultIsAscending: _this2.props.ascending,
          defaultNullsFirst: _this2.props.nullsfirst
        }]));
      });

      if (!Array.isArray(this._cachedSortedItems)) {
        this._cachedSortedItems = finalItems;
      }
    }

    /** @private */

  }, {
    key: '_getComparatorConfigsComparator',
    value: function _getComparatorConfigsComparator() {
      var comparatorsHash = (0, _objectHash.MD5)(this.props.comparators);

      if (comparatorsHash !== this._cachedGeneratedComparatorConfigHash) {
        this._cachedGeneratedComparator = _createComparatorFromComparatorConfigs(this.props.comparators);
        this._cachedGeneratedComparatorConfigHash = comparatorsHash;
      }

      return this._cachedGeneratedComparator;
    }

    // endregion

  }]);

  return Sort;
}(_react2.default.Component);

exports.default = Sort;


Sort.propTypes = {
  items: _propTypes2.default.array,

  render: _propTypes2.default.func,
  children: _propTypes2.default.func,

  sorter: _propTypes2.default.func,
  comparator: _propTypes2.default.func,
  comparators: _propTypes2.default.arrayOf(_propTypes2.default.oneOfType([_propTypes2.default.func, _propTypes2.default.oneOfType([_propTypes2.default.shape({
    comparator: _propTypes2.default.func.isRequired,
    options: _propTypes2.default.any
  }), _propTypes2.default.shape({
    propPath: _propTypes2.default.string.isRequired,
    type: _propTypes2.default.oneOf(['string', 'number', 'date', 'boolean']).isRequired,
    options: _propTypes2.default.shape({
      isAscending: _propTypes2.default.bool,
      nullsFirst: _propTypes2.default.bool,

      // region String Locale Comparison Options (only applicable when `type` === 'string')
      localeIds: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.arrayOf(_propTypes2.default.string)]),
      usage: _propTypes2.default.string,
      localeMatcher: _propTypes2.default.string,
      numeric: _propTypes2.default.bool,
      caseFirst: _propTypes2.default.string,
      sensitivity: _propTypes2.default.string,
      ignorePunctuation: _propTypes2.default.bool
      // endregion
    })
  })])])),

  ascending: _propTypes2.default.bool,
  nullsFirst: _propTypes2.default.bool,

  skip: _propTypes2.default.bool,
  ignoreCache: _propTypes2.default.bool
};

Sort.defaultProps = {
  sorter: _defaultSorter

  /* eslint-disable valid-jsdoc */

  // region Private Helpers

  /** @private */
};function _defaultSorter(array, comparator) {
  return array.sort(comparator);
}

/** @private */
function _createComparatorFromComparatorConfigs(comparatorConfigs) {
  var propComparators = comparatorConfigs.map(_createComparatorFromComparatorConfig);

  return function () {
    for (var i = 0; i < propComparators.length; i++) {
      var comparisonResult = propComparators[i].apply(propComparators, arguments);

      if (comparisonResult !== 0) {
        return comparisonResult;
      }
    }
  };
}

/** @private */
function _createComparatorFromComparatorConfig(comparatorConfig) {
  var comparator = comparatorConfig;

  if ((typeof comparatorConfig === 'undefined' ? 'undefined' : _typeof(comparatorConfig)) === 'object') {
    comparator = comparatorConfig.comparator != null ? _createFunctionComparatorWithOptions(comparatorConfig) : _createTypePropComparator(comparatorConfig);
  }

  return function (leftItem, rightItem, comparatorOptions) {
    var comparatorConfigOptions = comparatorConfig.options || {};

    var finalIsAscending = comparatorConfigOptions.isAscending == null ? comparatorOptions.defaultIsAscending : comparatorConfigOptions.isAscending;
    var result = comparator(leftItem, rightItem, _extends({}, comparatorConfigOptions, comparatorOptions));

    return finalIsAscending == null || finalIsAscending ? result : result * -1;
  };
}

/** @private */
function _createFunctionComparatorWithOptions(_ref2) {
  var comparator = _ref2.comparator,
      comparatorConfigOptions = _ref2.options;

  return function (leftItem, rightItem, comparatorOptions) {
    return comparator(leftItem, rightItem, _extends({}, comparatorConfigOptions, comparatorOptions));
  };
}

/** @private */
function _createTypePropComparator(_ref3) {
  var propPath = _ref3.propPath,
      type = _ref3.type,
      _ref3$options = _ref3.options;
  _ref3$options = _ref3$options === undefined ? {} : _ref3$options;

  var isAscending = _ref3$options.isAscending,
      nullsFirst = _ref3$options.nullsFirst,
      comparatorConfigOptions = _objectWithoutProperties(_ref3$options, ['isAscending', 'nullsFirst']);

  var comparator = null;

  switch (type) {
    case 'string':
      comparator = _stringComparator;
      break;

    case 'number':
      comparator = _numberComparator;
      break;

    case 'date':
      comparator = _dateComparator;
      break;

    case 'boolean':
      comparator = _booleanComparator;
      break;

    default:
      throw new Error('Comparator \'type\' given was invalid. Possible values are \'string\', \'number\', \'date\', and \'boolean\'.');
  }

  return function (leftItem, rightItem, comparatorOptions) {
    var defaultNullsFirst = comparatorOptions.defaultNullsFirst;


    if (leftItem === rightItem) {
      return 0;
    }

    var leftItemPropValue = (0, _getValue2.default)(leftItem, propPath);
    var rightItemPropValue = (0, _getValue2.default)(rightItem, propPath);

    if (leftItemPropValue === rightItemPropValue) {
      return 0;
    }

    var finalNullsFirst = nullsFirst == null ? defaultNullsFirst : nullsFirst;

    if (leftItem == null || leftItemPropValue == null) {
      return finalNullsFirst ? -1 : 1;
    }
    if (rightItem == null || rightItemPropValue == null) {
      return finalNullsFirst ? 1 : -1;
    }

    return comparator(leftItemPropValue, rightItemPropValue, _extends({}, comparatorConfigOptions, comparatorOptions));
  };
}

/** @private */
function _stringComparator(leftItemPropValue, rightItemPropValue, _ref4) {
  var localeIds = _ref4.localeIds,
      localeCompareOptions = _objectWithoutProperties(_ref4, ['localeIds']);

  return leftItemPropValue.localeCompare(rightItemPropValue, localeIds, localeCompareOptions);
}

/** @private */
function _numberComparator(leftItemPropValue, rightItemPropValue) {
  return Number(leftItemPropValue) - Number(rightItemPropValue);
}

/** @private */
function _dateComparator(leftItemPropValue, rightItemPropValue) {
  return new Date(leftItemPropValue) - new Date(rightItemPropValue);
}

/** @private */
function _booleanComparator(leftItemPropValue, rightItemPropValue) {
  return !Boolean(leftItemPropValue) - !Boolean(rightItemPropValue);
}

// endregion

//# sourceMappingURL=index.es5.js.map