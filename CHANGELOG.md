# 2.0.0

- **[BREAKING CHANGE]** Changed `react` and `prop-types` to be peer dependencies instead
  of normal dependencies.
- **[ADDED]** Support for `object-hash` >=1.3.0 or 2.x.
- Update some dev dependencies.




# 1.4.0

- **[New Feature]** Added ability to force a sort even if cached props have not changed
  via the `ignoreCache` prop. This is useful when a custom `comparator` has been provided
  that depends on information outside of the given `items` prop.



# 1.3.1

- **[New Feature]** Ability to pass function as `children` instead of using `render` prop.



# 1.2.1

- **[New Feature]** Added ability to skip sorting and just pass through given `items` via
  the new `skip` prop.



# 1.1.2

- **[New Feature]** Added ability to pass a "sorter" function to component. This basically
  allows a user to control the sorting algorithm used by the component when sorts occur.
  This is done via the `sort` prop.



# 1.0.0

- Initial release.
