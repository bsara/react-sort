# @bsara/react-sort [![NPM Package](https://img.shields.io/npm/v/@bsara/react-sort.svg?style=flat-square)][npm]

![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)

> **THIS LIBARY HAS BEEN DEPRECATED!** Use [react-optimized-sort](https://www.npmjs.com/package/react-optimized-sort) instead.

> A simple React component used for sorting collections and arrays.


[Storybook](https://bsara.gitlab.io/react-sort)

[Changelog](https://gitlab.com/bsara/react-sort/blob/master/CHANGELOG.md)



## Install

```bash
$ npm i --save @bsara/react-sort
```


## Usage

```jsx
import React from 'react';
import Sort from '@bsara/react-sort';
import { sort as timsort } from 'timsort';

const comparators = [
  { propPath: 'itemPropName1', type: 'string' },
  { propPath: 'itemPropName2', type: 'number' },
  { propPath: 'itemPropName3', type: 'date' },
  { propPath: 'itemPropName4', type: 'boolean' },

  {
    propPath: 'itemPropName5',
    options:  { myOptionProp: 42 },

    comparator(leftItem, rightItem, options) {
      // Perform comparison...
    }
  },

  function anotherComparator(leftItem, rightItem) {
    // Perform comparison...
  }
];

export default function MyComponent(props) {
  return (
    {/* ... */}

      <Sort
        items={props.items}
        sorter={timsort}
        comparators={comparators}
        render={(sortedItems) => (
          // Render sorted items...
        )} />

      {/* ...or... */}

      <Sort items={props.items}
            sorter={timsort}
            comparators={comparators}>
        {(sortedItems) => (
          // Render sorted items...
        )}
      </Sort>

    {/* ... */}
  );
}
```


## Props

- **items** `?Array`

    Array of items to be sorted and passed to `render`/`children` prop.

- **render(sortedItems)** or **children(sortedItems)** `!Function` - **REQUIRED**

    Render function (or function as child) called after `items` sorting is complete.

- **ascending** `?Boolean`

    Default option to sort in ascending or descending order.

- **nullsFirst** `?Boolean`

    Default option to sort with `null` values first or last.

- **skip** `?Boolean`

    When `true`, skips sorting and the value of `items` prop will be passed to `render`,
    unsorted. This is useful in the case where sorting may only be conditionally needed.

- **ignoreCache** `?Boolean`

    When `true`, a sort is forced even if none of the cached props have changed. This is
    useful when a custom `comparator` has been provided that depends on information
    outside of the given `items` prop.

- **sorter(array, comparator)** `?Function`

    > **NOTE:** If the sorter given does not return an `Array`, then it is assumed that
    > the array that was given to the sorter was modified.

    Specifying a `sorter` function allows you to essential control the sorting algorithm
    used when a sort occurs within the component. By default, `Array.prototype.sort` is
    used if no `sorter` is specified or if `sorter` is `null` or `undefined`.


- **comparator(leftItem, rightItem[, options])** `?Function`

    > **NOTE:** This prop takes precedence over the `comparators` prop and will cause the
    > `comparators` prop to be ignored if this props is set to a non-null, non-undefined
    > value.

    Comparator function to use when comparing the items found in the given `items` prop.

- **comparators** `?Array<(Function|Object)>`

    > **IMPORTANT:** When using this prop, it is assumed that all of the items given are
    > objects and not primitive values like a `string`, `boolean`, `date`, or `number`.

    > **NOTE:** This prop is ignored if the `comparator` prop is set to a non-null,
    > non-undefined value.

    A list of comparators and/or
    [comparator configurations](#comparator-configuration-properties) to use when
    comparing the items found in the given `items` prop. The comparators/configurations
    are applied in the order that they are found in the `comparators` array given. If an
    item in the `comparators` list is a `Function`, then that function will just be called
    with "left" and "right" parameters passed to it (in that order as well). If an item in
    the `comparators` list is an `Object`, then it will be treated as a "configuration".
    For more information about said "configurations", refer to
    [*Comparator Config Properties*](#comparator-configuration-properties) below.



## Comparator Configuration Properties

The comparator configuration objects can take one of two forms: either a `comparator` is
given with some accompanying `options`, or a `propPath` and `type` are given and
comparisons are taken care of for you by the `Sort` component. Below you will find two
tables, each describes the properties available for one of the two configuration objects
already described.

### PropPath/Type Configuration Properties:

> \* = Required Property

| Property Name/Type        | Type    | Default Value    | Description |
|:--------------------------|:--------|:-----------------|:------------|
| __*propPath__             | String  |                  | Path to item property to compare.
| __*type__                 | String  |                  | The type of the value found at `propPath`.<br/><br/>**Allowed Values:**<br/>string, number, date, boolean
| options                   | Object  |                  | Comparison options.
| options.isAscending       | Boolean | props.ascending  | When `true`, `items` will be sorted in ascending order.
| options.nullsFirst        | Boolean | props.nullsFirst | When `true`, `null` and `undefined` items will be placed before any defined item.
| options.localeIds         | String  |                  | Only used when `type` is `"string"`.<br/>*See [`localeCompare` docs][localeCompare-params-docs] for details.*
| options.usage             | String  |                  | Only used when `type` is `"string"`.<br/>*See [`localeCompare` docs][localeCompare-params-docs] for details.*
| options.localeMatcher     | String  |                  | Only used when `type` is `"string"`.<br/>*See [`localeCompare` docs][localeCompare-params-docs] for details.*
| options.numeric           | Boolean |                  | Only used when `type` is `"string"`.<br/>*See [`localeCompare` docs][localeCompare-params-docs] for details.*
| options.caseFirst         | String  |                  | Only used when `type` is `"string"`.<br/>*See [`localeCompare` docs][localeCompare-params-docs] for details.*
| options.sensitivity       | String  |                  | Only used when `type` is `"string"`.<br/>*See [`localeCompare` docs][localeCompare-params-docs] for details.*
| options.ignorePunctuation | Boolean |                  | Only used when `type` is `"string"`.<br/>*See [`localeCompare` docs][localeCompare-params-docs] for details.*

### Comparator Configuration Properties:

> \* = Required Property

| Property Name/Type  | Type     | Default Value   | Description |
|:--------------------|:---------|:----------------|:------------|
| __*comparator__     | Function |                 | Comparator function to use for comparisons.<br/><br/>**Function Signature:**<br/>`comparator(leftParam, rightParam, options)`
| options             | Object   |                 | Comparison options.<br/><br/>*(NOTE: `options` will be passed to `comparator` as a 3rd function parameter)*
| options.isAscending | Boolean  | props.ascending | When `true`, `items` will be sorted in ascending order.


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2019, Brandon D. Sara (https://bsara.dev/)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[license]: https://gitlab.com/bsara/react-sort/blob/master/LICENSE "License"
[npm]:     https://www.npmjs.com/package/@bsara/react-sort         "NPM Package: react-sort"

[localeCompare-params-docs]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare#Parameters "String.prototype.localeCompare (Parameters)"
